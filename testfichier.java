public class testfichier {

    public static int[][] Mat(int[][] tab1, int[][] tab2) {
        int[][] m = new int[tab1.length][tab2.length];
        for (int i =0; i< tab1.length; i++){
            for (int j=0; j< tab2.length; j++){
                m[i][j]= tab1[i][j] + tab2[i][j];
            }

        }
        return m;
    }

    public static int sommeDiagos(int[][] mat){
       int diago1=0;
       int diago2=0;
       int sommeDiago= 0;
       for (int i=0; i< mat.length; i++){
           for (int j=0; j<mat.length; j++){
               if (i==j){
                   diago1= mat[i][j] + diago1;

               }
               if (i+j== mat.length-1){
                   diago2= diago2 + mat[i][j];
               }
           }
       }
       sommeDiago= sommeDiago +diago2 +diago1;
        return sommeDiago;
    }

    public static int Matnull(int[][] mat){
        int cpt= 0;
        for (int i=0; i< mat.length; i++){
            for (int j=0; j< mat.length; j++){
                if (mat[i][j]==0){
                    cpt= cpt+1;
                }
            }
        } return cpt;
    }

    public static void Valnull(int[][] mat){
        if(Matnull(mat)==0){
            Ut.afficher("Il n'y a pas de valeur null");
        }
        else{
            Ut.afficher("Il y a au moins une valeur null");
        }

    }

    public static int ligneMat(int[][] mat) {
        int cpt = 0;
        int vmax = 0;
        int nbligne = 0;
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat.length; j++) {
                if (mat[i][j] == 0) {
                    cpt = cpt + 1;
                }
            }
            if (cpt > vmax) {
                vmax = cpt;
                nbligne = i ;
            }
        }
        return nbligne;
    }

    public static void plusvallnull(int[][] mat){
        int cpt =0;
        for (int j = 0; j < mat.length; j++){
            if (mat[ligneMat(mat)][j]== 0){
                cpt = cpt + 1;
            }
        }
        if (cpt >=2){
            Ut.afficher("il existe une ligne qui comporte plusieurs valeur null");
        }
        else{
            Ut.afficher("il n'existe pas de ligne comportant plusieurs valeur null");
        }


    }

    public static int[][] CarreMagique(int mat){
        int i, j, iNew, jNew;
        int[][] carre =new int [mat][mat];
        for (i=0; i< mat; i++){
            for (j=0; j< mat; j++){
                carre[i][j]= 0;

            }
        }
        i=0;
        j= mat/2;
        carre[i][j]= 1;
        for (int v= 2; v <= mat*mat; v++){
            iNew= Ut.modulo2((i -1),mat);
            jNew= Ut.modulo2((j +1),mat);
            if (carre[iNew][jNew] == 0){
                i= iNew;
                j= jNew;
            } else {
                i++;
            }
            carre[i][j] =v;
        }
        return carre;
    }

    public static int [][] Dimrangee(int n) {
        int Cpt= 1;
        int[][] rangee = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                rangee[i][j]= Cpt;
                Cpt= Cpt+1;

            }
        }
        return rangee;
    }

    public static int[][] Dimserpentin(int n){
        int Cpt= 1;
        int[][] serpentin = new int[n][n];
        for (int i = 0; i < n; i++) {
            if (i%2==0){
                for (int j=0; j < serpentin[0].length; j++){
                    serpentin[i][j]= Cpt;
                    Cpt++;
                }
            }
            else {
                for (int j= serpentin[0].length-1; j>=0 ; j--){
                    serpentin[i][j]= Cpt;
                    Cpt++;
                }
            }
        }
        return serpentin;
    }

    public static void main(String[] args) {
    int[][] tab1= {{1,2,3},{4,5,6},{7,8,9}};
    int[][] tab2= {{1,2,3},{4,5,6},{7,8,9}};
    //Ut.afficher(Mat(tab1,tab2));
    //Ut.afficher(sommeDiago4(tab1));
    //Ut.afficher(Matnull(tab1));
    //Valnull(tab1);
    //Ut.afficher(ligneMat(tab1));
    //plusvallnull(tab1);
    //Ut.afficher(CarreMagique(3));
    //Ut.afficher((Dimrangee(3)));
    Ut.afficher(Dimserpentin(4));
    }
}
